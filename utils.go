package main

// data type for email
type maildata struct {
	From       string
	Subject    string
	To         []string
	Attachment string
	Text       string
	Mx         string
}
