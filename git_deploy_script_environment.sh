#!/bin/bash

export AFTER_TYPE_CHECK="gobuild"
export USE_SEMANTIC="true"
export USE_HCIVERSION="true"
export SEMANTIC_COMMAND="./deploy.sh"
export HCIVERSION_SPECIAL_GO_BUILD="-ldflags='-extldflags=-static'"
export HCIVERSION_CGO_GO_BUILD="true"


function gobuild {
    CGO_ENABLED=0 go build -ldflags '-extldflags "-static"'
}