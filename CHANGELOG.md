## [1.2.9](https://gitlab.com/n0r1sk/smail/compare/v1.2.8...v1.2.9) (2022-06-30)


### Performance Improvements

* update dependencies ([aabfd51](https://gitlab.com/n0r1sk/smail/commit/aabfd514c444d4550c1941a7d4439d31bc9fd05e))

## [1.2.8](https://gitlab.com/n0r1sk/smail/compare/v1.2.7...v1.2.8) (2022-01-05)


### Bug Fixes

* fix deploy version ([143c7aa](https://gitlab.com/n0r1sk/smail/commit/143c7aab83bd93730e955fe684563c8c08b65ef4))

## [1.2.7](https://gitlab.com/n0r1sk/smail/compare/v1.2.6...v1.2.7) (2022-01-03)


### Performance Improvements

* update dependencies ([e0d9fb2](https://gitlab.com/n0r1sk/smail/commit/e0d9fb24d4e2438f942e08e242d058201e768806))

## [1.2.6](https://gitlab.com/n0r1sk/smail/compare/v1.2.5...v1.2.6) (2021-10-11)


### Performance Improvements

* update dependencies ([77ba313](https://gitlab.com/n0r1sk/smail/commit/77ba3136cd901f92c4f4f217602aa61626b49454))

## [1.2.5](https://gitlab.com/n0r1sk/smail/compare/v1.2.4...v1.2.5) (2021-08-09)


### Performance Improvements

* add text flag to smail ([aa3d477](https://gitlab.com/n0r1sk/smail/commit/aa3d477ee6db7f478aea0345a69af5f8e276c6b4))

## [1.2.4](https://gitlab.com/n0r1sk/smail/compare/v1.2.3...v1.2.4) (2021-08-09)


### Performance Improvements

* optimize for docker ([e6d9e3a](https://gitlab.com/n0r1sk/smail/commit/e6d9e3ac7421c1486c8358c305bd0b8eaf76cfe2))

## [1.2.3](https://gitlab.com/n0r1sk/smail/compare/v1.2.2...v1.2.3) (2021-08-09)


### Performance Improvements

* add Dockerfile ([3185efa](https://gitlab.com/n0r1sk/smail/commit/3185efaddb1d9ae6ac58ec627f13471e8fe7f21a))

## [1.2.2](https://gitlab.com/n0r1sk/smail/compare/v1.2.1...v1.2.2) (2021-08-09)


### Performance Improvements

* add hciversion ([86f55ee](https://gitlab.com/n0r1sk/smail/commit/86f55eea01a6c746dff6b5105e13efe0e29f7426))

## [1.2.1](https://gitlab.com/n0r1sk/smail/compare/v1.2.0...v1.2.1) (2021-08-09)


### Performance Improvements

* test semantic ([172318e](https://gitlab.com/n0r1sk/smail/commit/172318e2c13c7b845b0a0cdc57ab70b9d92be8fb))

# [1.2.0](https://gitlab.com/n0r1sk/smail/compare/v1.1.0...v1.2.0) (2021-08-09)


### Features

* reorganize code | now using kingpin instead of flags | try to use semantic ([1ecf930](https://gitlab.com/n0r1sk/smail/commit/1ecf930e7ebf7661e96199f6ae27de3003511dc5))


### Performance Improvements

* test semantic ([31e9851](https://gitlab.com/n0r1sk/smail/commit/31e9851bd9b665e5cf91ee0e25b2a1956aea6ef8))
* test semantic ([c47fba2](https://gitlab.com/n0r1sk/smail/commit/c47fba2d8da826bb828d9ff0ee1c42b16d9b2b9e))
* test semantic ([f2f9fc9](https://gitlab.com/n0r1sk/smail/commit/f2f9fc9cce95c1472d687b64a1db9a288d3f68a3))
* test semantic ([7c0180c](https://gitlab.com/n0r1sk/smail/commit/7c0180c32b2b0149327972af06b52fe4bc032f4b))
* test semantic ([db7ff1d](https://gitlab.com/n0r1sk/smail/commit/db7ff1d1c3ceeee0ff61bb82fec5d9f85814b093))
