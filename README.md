[![Build Status](https://travis-ci.org/n0r1sk/smail.svg?branch=master)](https://travis-ci.org/n0r1sk/smail)

# smail
Simple/Stupid/Short command line mailer (smail) binary for Ops usage written in golang with MX support

# Usage
./smail -s "test email" -t a@example.com -t a@example.com -f f@example.com -m mx.example.com -a /some/text/file

```
# ./smail --help
usage: smail --subject=SUBJECT --to=TO --from=FROM --mx=MX [<flags>]

command line tool for sending mails

Flags:
      --help                   Show context-sensitive help (also try --help-long and --help-man).
  -d, --debug                  Enable debug mode.
  -s, --subject=SUBJECT        Subject text
  -t, --to=TO ...              To address(es)
  -f, --from=FROM              From address
  -a, --attachment=ATTACHMENT  Attachment - textfile as body [optional]
  -b, --text=TEXT              Text - text as body [optional]
  -m, --mx=MX                  MX DNS Record
```

# Build information
You should build the go source for productive release via the following build command to retrieve a statically linked binary including all dependencies.

```
CGO_ENABLED=0 go build -ldflags '-extldflags "-static"'
```


# General information
The ```-m``` switch is mandatory!
