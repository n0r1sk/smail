module smail

go 1.18

require (
	git.app.strabag.com/dev/hostingci/gomodules/hcicore.git v1.8.1
	git.app.strabag.com/dev/hostingci/gomodules/hcielastic.git v1.6.1 // indirect
	git.app.strabag.com/dev/hostingci/gomodules/hcilog.git v1.8.1
	git.app.strabag.com/dev/hostingci/gomodules/hciversion.git v1.0.5
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/olivere/elastic/v7 v7.0.32 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)

require (
	github.com/josharian/intern v1.0.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
