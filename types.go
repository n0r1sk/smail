package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app      = kingpin.New("smail", "command line tool for sending mails")
	appDebug = app.Flag("debug", "Enable debug mode.").Short('d').Bool()

	subject    = app.Flag("subject", "Subject text").Short('s').Required().String()
	to         = app.Flag("to", "To address(es)").Short('t').Required().Strings()
	from       = app.Flag("from", "From address").Short('f').Required().String()
	attachment = app.Flag("attachment", "Attachment - textfile as body [optional]").Short('a').String()
	text       = app.Flag("body", "Text - text as body [optional]").Short('b').String()
	mx         = app.Flag("mx", "MX DNS Record").Short('m').Required().String()
)
