/*
Copyright 2017 Mario Kleinsasser and Bernhard Rausch

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"

	"git.app.strabag.com/dev/hostingci/gomodules/hcicore.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hcilog.git"
	"git.app.strabag.com/dev/hostingci/gomodules/hciversion.git"
	"gopkg.in/alecthomas/kingpin.v2"
	gomail "gopkg.in/gomail.v2"
)

func main() {
	hcilog.InitializeLog(app.Name, *appDebug)
	if hciversion.Number != "<undefined>" {
		hcilog.LogInfo("Version information: " + hciversion.Number)
	}
	kingpin.MustParse(app.Parse(os.Args[1:]))

	//Set up logrus
	*appDebug = hcilog.EvalDebugEnvironment(*appDebug)
	hcilog.InitializeLog(app.Name, *appDebug)

	// parse the command line flags
	md := maildata{
		From:       *from,
		Subject:    *subject,
		To:         *to,
		Attachment: *attachment,
		Text:       *text,
		Mx:         *mx,
	}

	hcilog.LogDebug(fmt.Sprintf("Given email parameters: %s", md))

	ew := sendmail(md)
	if ew != nil {
		ew.LogError()
	}
}

func sendmail(md maildata) *hcilog.ErrorWrapper {
	hcilog.LogInfo("Start mailing sequence.")

	var attachmenttext string

	if md.Attachment != "" {
		dat, err := ioutil.ReadFile(md.Attachment)
		if err != nil {
			return hcilog.NewErrorWrapper(err)
		}
		attachmenttext = string(dat[:])
		hcilog.LogDebug(attachmenttext)
	}

	if md.Text != "" {
		attachmenttext += hcicore.IfThenElse(attachmenttext != "", "\n"+md.Text, md.Text).(string)
	}

	// resolv the mx record
	resolved, err := resolveMx(md)
	if err != nil {
		return nil
	}

	for _, e := range resolved {
		for _, t := range md.To {
			err := send(md.From, t, md.Subject, &attachmenttext, e.Host)
			if err != nil {
				hcilog.LogWarn(fmt.Sprintf("Cant send email via: %s", e.Host))
				continue
			}
		}
		hcilog.LogInfo("Stopping mailing sequence.")
		return nil
	}

	return hcilog.NewErrorWrapper(errors.New("Cannot send mail via any smtpserver"))
}

func send(from string, to string, subject string, body *string, smtpserver string) *hcilog.ErrorWrapper {

	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", *body)

	d := &gomail.Dialer{Host: smtpserver, Port: 25}
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		return hcilog.NewErrorWrapper(err)
	}

	hcilog.LogInfo(fmt.Sprintf("Mail send to: %s", to))

	return nil
}

func resolveMx(md maildata) ([]*net.MX, *hcilog.ErrorWrapper) {

	resolved, err := net.LookupMX(md.Mx)
	if err != nil {
		hcilog.LogError(err)
		return resolved, hcilog.NewErrorWrapper(err)
	}

	return resolved, nil
}
